//
//  ViewController.swift
//  PermissionsSample
//
//  Created by Berkay Vurkan on 2.05.2020.
//  Copyright © 2020 Foo. All rights reserved.
//

import UIKit
import SPPermissions

class ViewController: UIViewController {
    
    let controller = SPPermissions.dialog([.camera, .photoLibrary, .locationWhenInUse, .locationAlwaysAndWhenInUse, .camera, .reminders, .notification])

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        initButton()
    }

    func initButton() {
        let btn = UIButton(frame: .init(x: 50, y: 50, width: 100, height: 30))
        btn.setTitle("İzinler", for: .normal)
        btn.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
        btn.setTitleColor(.black, for: .normal)
        view.addSubview(btn)
    }

    @objc func buttonTapped(_ sender: UIButton) {
        // Ovveride texts in controller
        controller.titleText = NSLocalizedString("Gerekli izinler", comment: "")
        controller.headerText = NSLocalizedString("İZİN İSTEKLERİ", comment: "")
        controller.footerText = NSLocalizedString("Uygulamayı kullanırken yukarıdaki listede belirtilen yönergelere erişim hakkı vermeniz gerekmektedir. Bu izinler aracılığıyla verilerinizi herhangi bir şekilde işleme hakkı vermiş olmazsınız. Verileriniz sizinle güvendedir.", comment: "")

        // Set `DataSource` or `Delegate` if need.
        // By default using project texts and icons.
        controller.dataSource = self
        controller.delegate = self

        // Always use this method for present
        controller.present(on: self)
    }
    
}

extension ViewController: SPPermissionsDelegate, SPPermissionsDataSource {
    
    func configure(_ cell: SPPermissionTableViewCell, for permission: SPPermission) -> SPPermissionTableViewCell {
        cell.permissionDescriptionLabel.text = permission.detail
        cell.button.allowTitle = NSLocalizedString("İZİN VER", comment: "")
        cell.button.allowedTitle = NSLocalizedString("İZİN VERİLDİ", comment: "")
        
        return cell
    }
    
    func deniedData(for permission: SPPermission) -> SPPermissionDeniedAlertData? {
        let data = SPPermissionDeniedAlertData()
        data.alertOpenSettingsDeniedPermissionTitle = NSLocalizedString(AppConstants.kPermissionDeniedTitle, comment: "")
        data.alertOpenSettingsDeniedPermissionDescription = NSLocalizedString(AppConstants.kPermissionDeniedDescription, comment: "")
        data.alertOpenSettingsDeniedPermissionButtonTitle = NSLocalizedString(AppConstants.kPermissionDeniedButtonTitle, comment: "")
        data.alertOpenSettingsDeniedPermissionCancelTitle = NSLocalizedString(AppConstants.kPermissionDeniedCancelTitle, comment: "")
        return data
    }
    
    func didDenied(permission: SPPermission) {
        
    }
    
}


extension SPPermission {
    
    var title: String {
        get {
            switch self {
            case .notification:
                return AppConstants.permissionNotificationTitle
            case .camera:
                return AppConstants.permissionCameraTitle
            case .locationWhenInUse:
                return AppConstants.permissionLocationWhenInUseTitle
            case .locationAlwaysAndWhenInUse:
                return AppConstants.permissionLocationAlwaysAndWhenInUseTitle
            case .photoLibrary:
                return AppConstants.permissionPhotoLibraryTitle
            case .reminders:
                return AppConstants.permissionRemindersTitle
            default:
                return ""
            }
        }
    }
    
    var detail: String {
        get {
            switch self {
            case .notification:
                return AppConstants.permissionNotificationDetail
            case .camera:
                return AppConstants.permissionCameraDetail
            case .locationWhenInUse:
                return AppConstants.permissionLocationWhenInUseDetail
            case .locationAlwaysAndWhenInUse:
                return AppConstants.permissionLocationAlwaysAndWhenInUseDetail
            case .photoLibrary:
                return AppConstants.permissionPhotoLibraryDetail
            case .reminders:
                return AppConstants.permissionRemindersDetail
            default:
                return ""
            }
        }
    }
    
}
