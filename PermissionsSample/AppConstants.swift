//
//  AppConstants.swift
//  PermissionsSample
//
//  Created by Berkay Vurkan on 2.05.2020.
//  Copyright © 2020 Foo. All rights reserved.
//

import Foundation


struct AppConstants {
    
    static let kPermissionDeniedTitle = "İzin reddedildi"
    static let kPermissionDeniedDescription = "Lütfen ayarlara gidiniz ve izin erişimi veriniz"
    static let kPermissionDeniedButtonTitle = "Ayarlar"
    static let kPermissionDeniedCancelTitle = "Vazgeç"
    
    static let permissionNotificationTitle = NSLocalizedString("", comment: "")
    static let permissionCameraTitle = NSLocalizedString("", comment: "")
    static let permissionLocationWhenInUseTitle = NSLocalizedString("", comment: "")
    static let permissionLocationAlwaysAndWhenInUseTitle = NSLocalizedString("", comment: "")
    static let permissionPhotoLibraryTitle = NSLocalizedString("Fotoğraflar", comment: "")
    static let permissionRemindersTitle = NSLocalizedString("Hatırlatıcı", comment: "")
    
    static let permissionNotificationDetail = NSLocalizedString("Seyahat ekranında, sürücülerle haberleşme kanalı olarak kullanıldığından izin verilmesi gereklidir", comment: "")
    static let permissionCameraDetail = NSLocalizedString("Profil veya araç fotoğrafı çekmek için kullanılır", comment: "")
    static let permissionLocationWhenInUseDetail = NSLocalizedString("Seyahat oluştururken ya da sürüş içeerisindeyseniz kullanılır", comment: "")
    static let permissionLocationAlwaysAndWhenInUseDetail = NSLocalizedString("Seyahat oluştururken ya da sürüş içeerisindeyseniz kullanılır. Eğer sürücüyseniz, bildirim istekleriniz açık olduğu takdirde daima konumunuza ihtiyaç duyarız", comment: "")
    static let permissionPhotoLibraryDetail = NSLocalizedString("Profile veya araç fotoğrafı yüklemek için kullanılır", comment: "")
    static let permissionRemindersDetail = NSLocalizedString("İleri bir tarihte seyahat yönergesi oluşturduysanız, size hatırlatmamızı sağlar", comment: "")
    
}
